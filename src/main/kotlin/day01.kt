//import kotlin.system.measureTimeMillis
//import kotlin.time.measureTime


fun main() {
    fun part1(input: List<String>): Int {
        //sumOf fait la somme de toutes les données de input, mais transformées
        //préalablement par la lambda
        return input.sumOf { cal ->
            val p0 = cal.first { it.isDigit() }.digitToInt()
            val p1 = cal.last { it.isDigit() }.digitToInt()
            p0 * 10 + p1
        }
    }

    fun part2(input: List<String>): Int {
        //on remplace les chiffre érit en lettres par leur digit associé, et ensuite on appelle part1
        val chiffres = listOf("one","two","three","four","five","six","seven","eight","nine")
        return input.map { cal ->
            //on cherche le premier chiffre écrit en lettre.
            cal.findAnyOf(chiffres)?.let { c1 ->
                //à noter: on ne remplace pas le dernier caractère qui peut être le dénut d"un chiffre suivant
                //par exemple dans eightwothree le premier t termine le eight mais débute le two
                //on le remplace donc par 8twothree
                val passe1 = cal.replaceRange(c1.first, c1.first+ c1.second.length-1, (chiffres.indexOf(c1.second)+1).toString())
                //on cherche désormais le dernier chiffre écrit en lettres restant
                //on le remplace par le digit associé
                passe1.findLastAnyOf(chiffres)?.let {c2 ->
                    passe1.replaceRange(c2.first, c2.first + c2.second.length, (chiffres.indexOf(c2.second)+1).toString())
                }?: passe1
            }?:cal
        }.let {
            //on a maintenant une liste équivalente de celle de la partie 1, donc on peut l'appeler
            part1(it)
        }
    }

    //val testInput = readInput("day01_test")
    //check(part1(testInput) == 1)
    val input = readInput("day01")
    println(part1(input))

    val input2 = readInput("day01_2")
    println(part2(input2))

//    val t2 = measureTimeMillis { println(part2(input)) }
//    val t2v2 = measureTimeMillis { println(part2(input)) }

//    println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}