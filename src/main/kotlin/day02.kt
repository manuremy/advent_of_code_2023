//import kotlin.system.measureTimeMillis
//import kotlin.time.measureTime

enum class Color (val valueMax: Int ){
    RED(12),
    GREEN(13),
    BLUE(14);
}

val re = Regex("(\\d+) (red|green|blue)")
fun main() {
    fun part1(input: List<String>): Int {
         return input.mapIndexed { index, game ->
            val tirage = game.substringAfter(":")
            val numGame = index +1
            if (re.findAll(tirage).map {
                it.destructured.let {
                        (value, color) -> color to value.toInt()
                }
            }
                .groupBy({it.first}, {it.second})
                .map { it.key to it.value.max() }
                .count {
                    it.second > Color.valueOf(it.first.uppercase()).valueMax
                } > 0) 0 else numGame
        }
        .sum()
    }

    fun part2(input: List<String>): Int {
        return input.sumOf { game ->
            val tirage = game.substringAfter(":")
            re.findAll(tirage)
                .map {
                    it.destructured.let { (value, color) ->
                        color to value.toInt()
                    }
                }
                .groupBy({ it.first }, { it.second })
                .map { it.value.max() }
                .let { it[0] * it[1] * it[2] }
        }
    }

    //val testInput = readInput("day01_test")
    //check(part1(testInput) == 1)
    val input = readInput("day02")
    println(part1(input))

    val input2 = readInput("day02")
    println(part2(input2))

//    val t2 = measureTimeMillis { println(part2(input)) }
//    val t2v2 = measureTimeMillis { println(part2(input)) }

//    println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}