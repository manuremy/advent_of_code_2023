//import kotlin.system.measureTimeMillis
//import kotlin.time.measureTime


fun inputModifie(input: List<String>) : List<String> = buildList {
    add(".".repeat(input[0].length + 2))
    addAll(input.map { ".$it." })
    add(".".repeat(input[0].length + 2))
}

val reJ3 = Regex("(\\d+)")

fun main() {
    fun part1(input: List<String>): Int {
        val l = inputModifie(input)
        val symbols =listOf("*","/","+","-","$","#","%","@","&","=")
        return l.mapIndexed { index: Int, ligne: String ->
            reJ3.findAll(ligne).map { mr ->
                val plage = mr.range.first-1..mr.range.last+1
                val matrice= l[index -1].substring(plage) + l[index].substring(plage) + l[index +1].substring(plage)
                matrice.findAnyOf(symbols)?.let { mr.value.toInt() }?:0
            }.sum()
        }.sum()
    }

    fun part2(input: List<String>): Int {
        val l = inputModifie(input)
        return l.asSequence().mapIndexed { numLigne: Int, ligne: String ->
            reJ3.findAll(ligne).map { mr ->
                val plage = mr.range.first-1..mr.range.last+1
                //+2 car l'index est exclusif
                l.subList(numLigne -1, numLigne +2).mapIndexed { l03: Int, s: String ->
                    s.substring(plage).mapIndexed { pos, c ->
                        if (c == '*') ((numLigne-1+l03 to plage.first + pos) to mr.value.toInt()) else null
                    }.filterNotNull()
                }.flatten()
            }.flatten()
        }
        .flatMap { it.toList() }
        .groupBy({ it.first }, {it.second})
        .mapNotNull{ if (it.value.size==2) it.value[0] * it.value[1] else null }
        .sum()
    }

    //val testInput = readInput("day01_test")
    //check(part1(testInput) == 1)
    val input = readInput("day03")
    println(part1(input))

    val input2 = readInput("day03")
    println(part2(input2))

//    val t2 = measureTimeMillis { println(part2(input)) }
//    val t2v2 = measureTimeMillis { println(part2(input)) }

//    println ("Temps t2: $t2 et temps t2_v2: $t2v2")


}